package tr.edu.sakarya.cs.mobileApp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import tr.edu.sakarya.cs.mobileApp.model.response.GetCategoriesResponse;
import tr.edu.sakarya.cs.mobileApp.service.CategoryService;

@Controller
@RequestMapping("/category")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public GetCategoriesResponse getCategories() {
        return categoryService.getAllCategories();
    }
}
