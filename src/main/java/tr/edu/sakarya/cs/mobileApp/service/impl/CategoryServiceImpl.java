package tr.edu.sakarya.cs.mobileApp.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tr.edu.sakarya.cs.mobileApp.model.CategoryDocument;
import tr.edu.sakarya.cs.mobileApp.model.response.GetCategoriesResponse;
import tr.edu.sakarya.cs.mobileApp.repository.CategoryRepository;
import tr.edu.sakarya.cs.mobileApp.service.CategoryService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    @Override
    public GetCategoriesResponse getAllCategories() {
        GetCategoriesResponse response = new GetCategoriesResponse();

        Collection<CategoryDocument> categories = iterateAndConvert(categoryRepository.findAll());
        response.setCategories(categories);

        return response;
    }

    private List<CategoryDocument> iterateAndConvert(Iterable<CategoryDocument> categoryDocumentIterable) {
        List<CategoryDocument> categoryDocuments = new ArrayList<>();
        categoryDocumentIterable.forEach(categoryDocuments::add);
        return categoryDocuments;
    }
}
