package tr.edu.sakarya.cs.mobileApp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import tr.edu.sakarya.cs.mobileApp.model.request.AdvertSaveRequest;
import tr.edu.sakarya.cs.mobileApp.model.response.BaseResponse;
import tr.edu.sakarya.cs.mobileApp.service.AdvertService;

@Controller
@RequestMapping("/advert")
public class AdvertController {

    @Autowired
    private AdvertService advertService;

    @RequestMapping(method = RequestMethod.POST, consumes = "application/json")
    @ResponseBody
    public BaseResponse save(@RequestBody AdvertSaveRequest request) {
        return advertService.save(request);
    }
}
