package tr.edu.sakarya.cs.mobileApp.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tr.edu.sakarya.cs.mobileApp.exception.MobileAppException;
import tr.edu.sakarya.cs.mobileApp.model.AdvertDocument;
import tr.edu.sakarya.cs.mobileApp.model.request.AdvertSaveRequest;
import tr.edu.sakarya.cs.mobileApp.model.response.BaseResponse;
import tr.edu.sakarya.cs.mobileApp.model.response.ErrorResponse;
import tr.edu.sakarya.cs.mobileApp.repository.AdvertRepository;
import tr.edu.sakarya.cs.mobileApp.service.AdvertService;

@Service
public class AdvertServiceImpl implements AdvertService {

    @Autowired
    private AdvertRepository advertRepository;

    @Override
    public BaseResponse save(AdvertSaveRequest advertSaveRequest) {
        BaseResponse response = new BaseResponse();
        try {
            advertSaveRequest.validate();

            AdvertDocument document = convertAdvertSaveRequestToAdvertDocument(advertSaveRequest);
            advertRepository.save(document);

            response.setSucceed(true);
        } catch (MobileAppException e) {
            setErrorToResponse(response, e);
        } finally {
            return response;
        }
    }

    private AdvertDocument convertAdvertSaveRequestToAdvertDocument(AdvertSaveRequest request) {
        AdvertDocument advert = new AdvertDocument();

        advert.setCategory(request.getCategory());
        advert.setDescription(request.getDescription());
        advert.setTitle(request.getTitle());
        advert.setPrice(request.getPrice());

        return advert;
    }

    private void setErrorToResponse(BaseResponse response, MobileAppException e) {
        response.setSucceed(false);

        ErrorResponse error = initErrorResponseWithMobileAppException(e);
        response.setError(error);
    }

    private ErrorResponse initErrorResponseWithMobileAppException(MobileAppException e) {
        ErrorResponse errorResponse = new ErrorResponse();

        errorResponse.setKey(e.getKey());
        errorResponse.setMessage(e.getMessage());

        return errorResponse;
    }
}
