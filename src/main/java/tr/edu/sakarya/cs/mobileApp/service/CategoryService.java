package tr.edu.sakarya.cs.mobileApp.service;

import tr.edu.sakarya.cs.mobileApp.model.response.GetCategoriesResponse;

public interface CategoryService {

    GetCategoriesResponse getAllCategories();
}
