package tr.edu.sakarya.cs.mobileApp.model.request;

import tr.edu.sakarya.cs.mobileApp.exception.MobileAppException;

import java.math.BigDecimal;

import static tr.edu.sakarya.cs.mobileApp.exception.Const.BASE_VALIDATION_EXCEPTION_MESSAGE;

public class AdvertSaveRequest {

    private String title;
    private String description;
    private String category;
    private BigDecimal price;

    public void validate() throws MobileAppException {
        if (title.isEmpty()) {
            throw new MobileAppException(BASE_VALIDATION_EXCEPTION_MESSAGE, "title");
        }

        if (description.isEmpty()) {
            throw new MobileAppException(BASE_VALIDATION_EXCEPTION_MESSAGE, "description");
        }

        if (category.isEmpty()) {
            throw new MobileAppException(BASE_VALIDATION_EXCEPTION_MESSAGE, "category");
        }

        if (price == null || price.doubleValue() < 0.1) {
            throw new MobileAppException(BASE_VALIDATION_EXCEPTION_MESSAGE, "price");
        }
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
