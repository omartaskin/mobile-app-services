package tr.edu.sakarya.cs.mobileApp.exception;

public class MobileAppException extends Exception {

    private String message;
    private String key;

    public MobileAppException(String message, String key) {
        super(message);

        this.key = key;
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
