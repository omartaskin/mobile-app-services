package tr.edu.sakarya.cs.mobileApp.model.response;

import tr.edu.sakarya.cs.mobileApp.model.CategoryDocument;

import java.util.Collection;

public class GetCategoriesResponse extends BaseResponse {

    private Collection<CategoryDocument> categories;

    public Collection<CategoryDocument> getCategories() {
        return categories;
    }

    public void setCategories(Collection<CategoryDocument> categories) {
        this.categories = categories;
    }
}
