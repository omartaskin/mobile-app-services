package tr.edu.sakarya.cs.mobileApp.repository;

import org.springframework.data.repository.CrudRepository;
import tr.edu.sakarya.cs.mobileApp.model.AdvertDocument;

public interface AdvertRepository extends CrudRepository<AdvertDocument, Long> {

}
