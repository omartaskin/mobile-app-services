package tr.edu.sakarya.cs.mobileApp.service;

import tr.edu.sakarya.cs.mobileApp.model.request.AdvertSaveRequest;
import tr.edu.sakarya.cs.mobileApp.model.response.BaseResponse;

public interface AdvertService {

    BaseResponse save(AdvertSaveRequest advertSaveRequest);
}
