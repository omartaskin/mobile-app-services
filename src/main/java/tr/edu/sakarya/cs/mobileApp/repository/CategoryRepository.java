package tr.edu.sakarya.cs.mobileApp.repository;

import org.springframework.data.repository.CrudRepository;
import tr.edu.sakarya.cs.mobileApp.model.CategoryDocument;

public interface CategoryRepository extends CrudRepository<CategoryDocument, Long> {

}
